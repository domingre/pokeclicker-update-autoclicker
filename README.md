# PokeClicker Update Autoclicker

Added the new functionality to the scripts of the [Ephenia](https://github.com/Ephenia/Pokeclicker-Scripts)

These are originally created and intended for the use of the browser extension known as [Tampermonkey](https://www.tampermonkey.net/).

I don't want to appropriate [Ephenia](https://github.com/Ephenia/Pokeclicker-Scripts) work,<br>
i wanted to bring my player needs to her storyline, and instead of keeping it to myself, __sharing it__.

For the installation, with the [Tampermonkey](https://www.tampermonkey.net/) extension,<br>
if you have the [Ephenia](https://github.com/Ephenia/Pokeclicker-Scripts) script named enhancedautoclicker.user.js,<br>
you must replace the code that the file contains with the one that I propose update_enhancedautoclicker.user.js<br>
Otherwise, just add a new script and place the code from the file there.

```diff
- Note: Please backup your saves before using any and all scripts that would be here!!!
- Note: Feel free to open an issue if you find any bugs/issues as these aren't fully tested!!!
- Note: in case it isn't mention below, all user set settings with these scripts are saved and persist even upon game close!!!
```

## New feature

This script was originally created by Ivan Lay and can be found over here.<br>
This script currently features quite a lot more over the old one.

You can find four new buttons there.

* [**1 Battle** ](//gitlab.com/domingre/test#user-content-1-battle-and-10-battle-button)
* [**10 Battle** ](//gitlab.com/domingre/test#user-content-1-and-10-button)
* [**Input Texte** ](//gitlab.com/domingre/test#user-content-input-texte)
* [**Cought All Pokemon Dungeon** ](//gitlab.com/domingre/test#user-content-cought-all-pokemon-dungeon)


![image feature](https://i.postimg.cc/t4CZDprk/new-interface.jpg)

## 1 Battle and 10 Battle button

It allows to define the number of dungeons to do, without having to write in text input.<br>
The basic numbers are therefore 1 and 10.

## Input Texte

This box allows you to define the number of times you want to do a dungeon.<br>
Then you just have to click on Auto Dungeon launched it.<br>
Above, you will be able to find the number of dungeons that remain to be done compared to the number previously defined.

![image feature](https://i.postimg.cc/VvsybGR7/Sans-azeazeae.png)

## Cought All Pokemon Dungeon

__This button can only be activated on a dungeon of which you have not captured all the pokemons.__
![image feature](https://i.postimg.cc/6qpSQ7Wg/Sans-titreazeaze.png)
<br>Once activated, this option will allow you to continue fighting until you have managed to capture all the pokemon in the dungeon.<br>

Once all the pokemon have been captured, the auto dungeon stops.<br>
However, if the number chosen in the input text is reached, the auto battle also stops.<br>

![image feature](https://i.postimg.cc/j2j7kTcy/Caaaaapture.jpg) ![image feature](https://i.postimg.cc/0jJ8NH2L/Captaaaaure.jpg)




